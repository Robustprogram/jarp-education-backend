"""
  Register models into the administration manager
"""
from django.contrib import admin
from .models import Course, Page, Question, Option

admin.site.register(Course)
admin.site.register(Page)
admin.site.register(Question)
admin.site.register(Option)
