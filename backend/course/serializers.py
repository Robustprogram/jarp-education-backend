"""
    Serialise the models all related to course models
"""
from rest_framework import serializers
from .models import Course, Page, Question, Option


class CourseSerializer(serializers.ModelSerializer):
    """ Serialise the course data """
    class Meta:
        model = Course
        fields = '__all__'


class PageSerializer(serializers.ModelSerializer):
    """ Serialise page data """
    class Meta:
        model = Page
        fields = '__all__'


class QuestionSerializer(serializers.ModelSerializer):
    """ Serialise question data """
    class Meta:
        model = Question
        fields = '__all__'


class OptionSerializer(serializers.ModelSerializer):
    """ Serialise option data """
    class Meta:
        model = Option
        fields = '__all__'
