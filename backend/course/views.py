"""
    Views to allow for the resources to be accessed via API
"""
from rest_framework import generics
from .models import Course, Page, Question, Option
from .serializers import CourseSerializer, PageSerializer, QuestionSerializer, OptionSerializer


class CourseList(generics.ListCreateAPIView):
    """ Show all of the courses and allow you to create a course """
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class PageList(generics.ListCreateAPIView):
    """ Show all of the pages and allow you to create a page """
    queryset = Page.objects.all()
    serializer_class = PageSerializer


class QuestionList(generics.ListCreateAPIView):
    """ Show all of the questions and allow you to create a question """
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class OptionList(generics.ListCreateAPIView):
    """ Show all of the options and allow you to create a option """
    queryset = Option.objects.all()
    serializer_class = OptionSerializer
