"""
    Provide links to the models related to the courses
"""
from django.urls import path
from . import views

urlpatterns = [
    path('course/', views.CourseList.as_view()),
    path('page/', views.PageList.as_view()),
    path('question/', views.QuestionList.as_view()),
    path('option/', views.OptionList.as_view()),
]
