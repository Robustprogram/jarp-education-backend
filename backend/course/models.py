"""
    Models for the course app
"""
from django.db import models
from django.core.validators import MaxValueValidator


class Course(models.Model):
    """ Course, which holds everything """
    course_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=2000, blank=True, default='')
    pass_percent = models.PositiveSmallIntegerField(
        help_text='The percentage you need to get for the final mark to pass.',
        validators=[MaxValueValidator(100), ],
        default='50',
    )


class Page(models.Model):
    """ A page of a course """
    page_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    content = models.TextField(blank=True, default='')
    next_page = models.ForeignKey(
        'self',
        null=True,
        on_delete=models.CASCADE,
        related_name='fk_next_page',
        default=None,
        help_text='The page next in line after this!'
    )
    prev_page = models.ForeignKey(
        'self',
        null=True,
        on_delete=models.CASCADE,
        related_name='fk_prev_page',
        default=None,
        help_text='The page that goes before this!'
    )
    course = models.ForeignKey(
        Course,
        null=True,
        on_delete=models.CASCADE,
        default=None,
        help_text='Course the page is allocated to'
    )


class Question(models.Model):
    """ Model laying out how a question should be represented """
    question_id = models.AutoField(primary_key=True)
    content = models.CharField(max_length=500)
    assigned_page = models.ForeignKey(Page, on_delete=models.CASCADE)


class Option(models.Model):
    """ A choice you can make to answer a question """
    option_id = models.AutoField(primary_key=True)
    assigned_question = models.ForeignKey(Question, on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField(default=0)
    correct = models.BooleanField(default=False)
